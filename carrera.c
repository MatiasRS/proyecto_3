#include "listcarrera.h"
#include <stdio.h>
#include <stdlib.h>

void cargar_listado(char path[], uv carrera[]){//esta funcion permite cargar los datos de las carreras
	FILE *file;
	int i = 0;
	if((file=fopen(path,"r"))==NULL){
		printf("error al arbir el archivo");
		exit(0); 
	}
	else{
		while (feof(file) == 0) {
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %d %f %f %d %d" ,&carrera[i].nombre ,&carrera[i].codigo, &carrera[i].NEM, &carrera[i].RANK, &carrera[i].LENG, &carrera[i].MAT, &carrera[i].HIST,&carrera[i].Cond, &carrera[i].CS, &carrera[i].POND, &carrera[i].PSU, &carrera[i].MAX, &carrera[i].MIN, &carrera[i].psu, &carrera[i].BEA);
			i++;

		}
		fclose(file);
	} 
}

